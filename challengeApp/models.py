from django.db import models

class Product (models.Model):
 name = models.CharField(max_length=30)
 code = models.CharField(max_length=30)
 bstbfr = models.Charfield(max_length=10)
 ntrvlr= models.Charfield(max_length=100)
 cost = models.DecimalField()
 weight = models.DecimalField()

 def __str__(self):
    return self.name
