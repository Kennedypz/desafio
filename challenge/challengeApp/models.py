from django.db import models

class Product (models.Model):
 name = models.CharField(max_length=30)
 code = models.CharField(max_length=30)
 bstbfr = models.CharField(max_length=10)
 ntrvlr= models.CharField(max_length=100)
 cost = models.CharField(max_length=5)
 weight = models.CharField(max_length=5)

 def __str__(self):
    return self.name
