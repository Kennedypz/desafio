from django.urls import path
from .views import *

urlpatterns = [
 path('home/', product_list, name='home_list'),
 path('new/', product_new, name='product_new'),
 path('update/<int:id>/', product_update, name='product_update'),
 path('delete/<int:id>/', product_delete, name='product_delete'),
 ]